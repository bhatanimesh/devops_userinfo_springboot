FROM openjdk:11-jre-slim
WORKDIR /app

ENV DB_USERNAME admin
ENV DB_PASSWORD password
ENV DB_HOST database-1.civjwwgvwy0h.us-east-1.rds.amazonaws.com
ENV DB_NAME userinfo
ENV DB_PORT 3306

COPY target/springboot-k8s-mysql-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080

CMD ["java", "-jar", "app.jar"]
